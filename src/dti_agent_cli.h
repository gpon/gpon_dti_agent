#ifndef _DTI_AGENT_CLI_H
#define _DTI_AGENT_CLI_H
/******************************************************************************

                              Copyright (c) 2011
                            Lantiq Deutschland GmbH
                     Am Campeon 3; 85579 Neubiberg, Germany

  For licensing information, see the file 'LICENSE' in the root folder of
  this software module.

******************************************************************************/

/** \file
   Debug and Trace Interface - Command Line Interface (CLI) definitions and
   declarations.
*/

#ifdef __cplusplus
extern "C" {
#endif

/* ============================================================================
   Includes
   ========================================================================= */
#include "ifx_types.h"
#include "dti_device.h"
#include "dti_agent_interface.h"
#include "dti_cli_interface.h"

/* ============================================================================
   Defines
   ========================================================================= */

/* ============================================================================
   Typedefs
   ========================================================================= */

/**
   CLI Ctx handle
*/
struct dti_cli_ctx {
	/** keeps the name of the given device name */
	IFX_char_t dev_name[DTI_MAX_LEN_DEVICE_INTERFACE_NAME];

	/** devices fd sending CLI-IOCTL */
	IFX_int_t dev_fd;

	/** CLI interface index */
	IFX_int_t cli_interface;
};

/* ============================================================================
   Exports
   ========================================================================= */

/**
   This function registers a corresponding send function within the DTI.
   The function is used to send incoming command strings to the caller
   command line interface (CLI).
   The send function must have the format of \ref DTI_CliSendCommandFunc_t

\param
   pDtiAgentContext  - points to the DTI Agent context.
\param
   dev_name          - GPON Optic device base name
\param
   cli_if_name        - CLI name, via this name the CLI will be identified from
                       the remote client.
\param
   send_fct          - function pointer to the send function to send a command
                       on this CLI.
                       Provide a NULL pointer to reserve the CLI interface and
                       to get a interface number (for example, only to register
                       an event call back).
\param
   resp_buf_sz - the buffer size required by the callback function.

\return
   In case of success, the CLI interface is marked as reserved and the
   interface number is returned (>= 0)
   In case of error IFX_ERROR will be return.

\remarks
   The last available interface is the default loop back interface.
*/
extern IFX_int_t dti_cli_add(DTI_AgentCtx_t * agent_ctx,
			     const IFX_char_t *dev_name,
			     const IFX_char_t *cli_if_name,
			     DTI_CliSendCommandFunc_t send_fct,
			     IFX_uint_t resp_buf_sz);

#ifdef __cplusplus
}
#endif
#endif				/* #ifndef _DTI_AGENT_CLI_H */
