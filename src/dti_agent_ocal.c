/******************************************************************************

                              Copyright (c) 2011
                            Lantiq Deutschland GmbH
                     Am Campeon 3; 85579 Neubiberg, Germany

  For licensing information, see the file 'LICENSE' in the root folder of
  this software module.

******************************************************************************/

/** \file
   Debug and Trace Interface - Basic Protocol Packet Handling.
*/

/* ============================================================================
   Includes
   ========================================================================= */
#include "dti_osmap.h"

#if defined(DEVICE_GPON_OCAL)

#include "ifx_dti_protocol.h"
#include "ifx_dti_protocol_device.h"
#include "ifx_dti_protocol_cli.h"

#include "dti_device.h"
#include "dti_agent_optic.h"
#include "dti_agent_ocal.h"
#include "dti_agent_ocal_ctx.h"
#include "dti_agent_cli.h"

#include "drv_optic_interface.h"

#include "ocal_api.h"

/* ============================================================================
   Defines
   ========================================================================= */

#ifdef DTI_STATIC
#  undef DTI_STATIC
#endif

#ifdef DTI_DEBUG
#  define DTI_STATIC
#else
#  define DTI_STATIC       static
#endif

#ifndef _IOWR
#  define _IOWR _IOW
#endif

/* ============================================================================
   Local Function Declaration
   ========================================================================= */

DTI_STATIC IFX_int_t dti_ocal_dev_open(IFX_char_t *dev_name);

DTI_STATIC IFX_int_t dti_ocal_dev_close(IFX_int_t dev_fd);

DTI_STATIC IFX_int_t dti_ocal_cntrl_port_open(
				struct dti_dev_gpon_ocal_ctx *dti_ocal_ctx,
				IFX_int_t dev_num,
				DTI_PacketError_t *packet_err);

DTI_STATIC IFX_int_t dti_ocal_cntrl_port_close(
				struct dti_dev_gpon_ocal_ctx *dti_ocal_ctx,
				IFX_int_t dev_num,
				DTI_PacketError_t *packet_err);

DTI_STATIC IFX_void_t dti_ocal_dev_fd_clear(
				struct dti_dev_gpon_ocal_ctx *dti_ocal_ctx,
				IFX_int_t dev_num);

DTI_STATIC IFX_int_t dti_ocal_dev_cfg_get(DTI_DeviceSysInfo_t *sys_info);

/* ============================================================================
   Device Access Functions
   ========================================================================= */
DTI_STATIC IFX_int_t dti_ocal_printout_level_set(IFX_int_t new_dbg_level);

DTI_STATIC IFX_int_t dti_ocal_module_setup(DTI_DeviceSysInfo_t *dev_sys_info,
					  DTI_DeviceCtx_t **dti_dev_ctx_out);

DTI_STATIC IFX_int_t dti_ocal_module_delete(DTI_DeviceSysInfo_t *dev_sys_info,
					   DTI_DeviceCtx_t **dti_dev_ctx_out);

DTI_STATIC IFX_int_t dti_ocal_sys_info_write(DTI_DeviceSysInfo_t *dev_sys_info,
					    IFX_char_t *sys_info_buf,
					    IFX_int_t buffer_size);

DTI_STATIC IFX_int_t dti_ocal_reset(DTI_DeviceCtx_t *dti_dev_ctx,
				   DTI_H2D_DeviceReset_t *in_dev_reset,
				   DTI_D2H_DeviceReset_t *out_dev_reset,
				   IFX_int_t rst_mask_size_32,
				   DTI_PacketError_t *packet_err);

DTI_STATIC IFX_int_t dti_ocal_download(DTI_DeviceCtx_t *dti_dev_ctx,
				      DTI_ProtocolServerCtx_t *dti_prot_srv_ctx,
				      DTI_H2D_DeviceDownload_t *in_dev_dwnld,
				      DTI_D2H_DeviceDownload_t *out_dev_dwnld,
				      DTI_PacketError_t *packet_err);

DTI_STATIC IFX_int_t dti_ocal_device_open(DTI_DeviceCtx_t *dti_dev_ctx,
					 IFX_int_t dev_num,
					 DTI_PacketError_t *packet_err);

DTI_STATIC IFX_int_t dti_ocal_device_close(DTI_DeviceCtx_t *dti_dev_ctx,
					  IFX_int_t dev_num,
					  DTI_PacketError_t *packet_err);

DTI_STATIC IFX_int_t dti_ocal_register_lock(DTI_DeviceCtx_t *dti_dev_ctx,
					   DTI_H2D_DeviceLock_t *in_lock,
					   DTI_D2H_DeviceLock_t *out_lock,
					   IFX_int_t dev_num,
					   DTI_PacketError_t *packet_err);

DTI_STATIC IFX_int_t dti_ocal_register_get(DTI_DeviceCtx_t *dti_dev_ctx,
					  DTI_H2D_RegisterGet_t *in_reg_get,
					  DTI_D2H_RegisterGet_t *out_reg_get,
					  IFX_int_t dev_num,
					  IFX_uint32_t *out_payl_sz_byte,
					  DTI_PacketError_t *packet_err);

DTI_STATIC IFX_int_t dti_ocal_register_set(DTI_DeviceCtx_t *dti_dev_ctx,
					  DTI_H2D_RegisterSet_t *in_reg_set,
					  IFX_int_t in_reg_set_sz_byte,
					  IFX_int_t dev_num,
					  DTI_PacketError_t *packet_err);

DTI_STATIC IFX_int_t dti_ocal_cfg_set(DTI_DeviceCtx_t *dti_dev_ctx,
					DTI_H2D_DeviceConfigSet_t *in_cfg_set,
					DTI_D2H_DeviceConfigSet_t *out_cfg_set,
					IFX_int_t dev_num,
					DTI_PacketError_t *packet_err);

DTI_STATIC IFX_int_t dti_ocal_cfg_get(DTI_DeviceCtx_t *dti_dev_ctx,
					DTI_H2D_DeviceConfigGet_t *in_cfg_get,
					DTI_D2H_DeviceConfigGet_t *out_cfg_get,
					IFX_int_t dev_num,
					DTI_PacketError_t *packet_err);

DTI_STATIC IFX_int_t dti_ocal_msg8_send(DTI_DeviceCtx_t *dti_dev_ctx,
					   DTI_H2D_Message8_u *in_msg8_send,
					   DTI_D2H_Message8_u *out_msg8_send,
					   IFX_int_t dev_num,
					   IFX_int_t in_payl_sz_byte,
					   IFX_int_t *out_payl_sz_byte,
					   DTI_PacketError_t *packet_err);

DTI_STATIC IFX_int_t dti_ocal_msg16_send(DTI_DeviceCtx_t *dti_dev_ctx,
					    DTI_H2D_Message16_u *in_msg16_send,
					    DTI_D2H_Message16_u *out_msg16_send,
					    IFX_int_t dev_num,
					    IFX_int_t in_payl_sz_byte,
					    IFX_int_t *out_payl_sz_byte,
					    DTI_PacketError_t *packet_err);

DTI_STATIC IFX_int_t dti_ocal_msg32_send(DTI_DeviceCtx_t *dti_dev_ctx,
					    DTI_H2D_Message32_u *in_msg32_send,
					    DTI_D2H_Message32_u *out_msg32_send,
					    IFX_int_t dev_num,
					    IFX_int_t in_payl_sz_byte,
					    IFX_int_t *out_payl_sz_byte,
					    DTI_PacketError_t *packet_err);

DTI_STATIC IFX_int_t dti_ocal_trace_buf_cfg_set(DTI_DeviceCtx_t *dti_dev_ctx,
					       DTI_H2D_TraceConfigSet_t
					       *in_trace_config_set,
					       DTI_D2H_TraceConfigSet_t
					       *out_trace_config_set,
					       IFX_int_t dev_num,
					       DTI_PacketError_t *packet_err);

DTI_STATIC IFX_int_t dti_ocal_trace_buf_reset(DTI_DeviceCtx_t *dti_dev_ctx,
						IFX_int_t dev_num,
						DTI_PacketError_t *packet_err);

DTI_STATIC IFX_int_t dti_ocal_trace_buf_status_get(DTI_DeviceCtx_t *dti_dev_ctx,
						  DTI_D2H_TraceStatusGet_t
						  *out_trace_status_get,
						  IFX_int_t dev_num,
						  DTI_PacketError_t
						  *packet_err);

DTI_STATIC IFX_int_t dti_ocal_trace_buf_get(DTI_DeviceCtx_t *dti_dev_ctx,
					   DTI_H2D_TraceBufferGet_t
					   *in_trace_buf_get,
					   DTI_Packet_t **used_dti_packet_out,
					   IFX_int_t *used_buffer_out_size,
					   IFX_int_t dev_num,
					   IFX_int_t *tr_buf_read_sz_byte,
					   DTI_PacketError_t *packet_err);

DTI_STATIC IFX_int_t dti_ocal_debug_read(DTI_DeviceCtx_t *dti_dev_ctx,
					DTI_H2D_DebugRead_t *in_dbg_get,
					DTI_D2H_DebugRead_t *out_dbg_get,
					IFX_int_t dev_num,
					IFX_int_t *dbg_read_count,
					DTI_PacketError_t *packet_err);

DTI_STATIC IFX_int_t dti_ocal_debug_write(DTI_DeviceCtx_t *dti_dev_ctx,
					 DTI_H2D_DebugWrite_t *in_dbg_set,
					 IFX_uint32_t *out_dbg_get_nu,
					 IFX_int_t dev_num,
					 IFX_int_t *dbg_write_count,
					 DTI_PacketError_t *packet_err);

DTI_STATIC IFX_int_t dti_ocal_auto_msg_process(DTI_DeviceCtx_t *dti_dev_ctx,
					      const DTI_Connection_t *dti_con,
					      IFX_uint32_t dev_select_wait_ms,
					      IFX_char_t *out_buffer,
					      IFX_int_t out_buffer_sz_byte);

DTI_STATIC IFX_int_t dti_ocal_win_easy_cli_access(DTI_DeviceCtx_t *dti_dev_ctx,
						 IFX_int_t dev_num,
						 const IFX_uint8_t *data_in,
						 const IFX_uint32_t size_in,
						 IFX_uint8_t *data_out,
						 const IFX_uint32_t size_out,
						 DTI_PacketError_t *packet_err);

/* ============================================================================
   Variables
   ========================================================================= */

/* Create device debug module - user part */
IFXOS_PRN_USR_MODULE_CREATE(DTI_GPON_OCAL, DTI_PRN_LEVEL_HIGH);

#if (DTI_DEVICE_INTERFACE_VERSION != 0x00010200)
#error "wrong DTI_DEVICE_INTERFACE_VERSION!"
#endif

/**
   Collection of all available device access functions.
*/
DTI_DeviceAccessFct_t dti_dev_access_fct_gpon_ocal = {
	sizeof(DTI_DeviceAccessFct_t),	/* structure check */
	DTI_DEVICE_INTERFACE_VERSION,	/* device interface version */
	DTI_DEV_GPON_OCAL_NAME,	        /* device name */

	dti_ocal_printout_level_set,
	dti_ocal_module_setup,
	dti_ocal_module_delete,
	dti_ocal_sys_info_write,

	dti_ocal_reset,
	dti_ocal_download,
	dti_ocal_device_open,
	dti_ocal_device_close,
	dti_ocal_register_lock,
	dti_ocal_register_get,
	dti_ocal_register_set,
	dti_ocal_cfg_set,
	dti_ocal_cfg_get,
	dti_ocal_msg16_send,
	dti_ocal_msg32_send,
	dti_ocal_trace_buf_cfg_set,
	dti_ocal_trace_buf_reset,
	dti_ocal_trace_buf_status_get,
	dti_ocal_trace_buf_get,
	dti_ocal_debug_read,
	dti_ocal_debug_write,
	dti_ocal_auto_msg_process,
	dti_ocal_win_easy_cli_access,

#if (DTI_DEVICE_INTERFACE_VERSION > 0x00000100)
	dti_ocal_msg8_send,
#endif
};

/* ============================================================================
   Local Function
   ========================================================================= */

/**
   Open a control device of the GPON Optic driver

\param
   dev_num      - debug device number
\param
   dev_name     - GPON Optic device base name

\return
   if success, a valid device fd
   else IFX_ERROR.
*/
DTI_STATIC IFX_int_t dti_ocal_dev_open(IFX_char_t * dev_name)
{
	IFX_int_t dev_fd = -1, conn_num;
	IFX_char_t buf[128];

	for (conn_num = DTI_GPON_OPTIC_START_ID;
	     conn_num <= DTI_GPON_OPTIC_MAX_ID; conn_num++) {
		/* /dev/gpon/<conn num> */
		(void)DTI_snprintf(buf, 128, "%s%d", dev_name, conn_num);
		dev_fd = DTI_DeviceOpen(buf);
		if (dev_fd < 0) {
			DTI_PRN_USR_ERR_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_ERR,
					   ("WRN: GPON OCAL Dev:"
					   " Optic Dev Open - <%s> "
					   "can't open device."DTI_CRLF, buf));

			/* /dev/gpon/<conn num> */
			(void)DTI_snprintf(buf, 128, "%s/%d", dev_name,
					   conn_num);
			dev_fd = DTI_DeviceOpen(buf);
		}

		if (dev_fd < 0) {
			DTI_PRN_USR_ERR_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_ERR,
					   ("WRN: GPON OCAL Dev:"
					   " Optic Dev Open - <%s> "
					   "can't open device."DTI_CRLF, buf));
		} else {
			DTI_PRN_USR_DBG_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_HIGH,
					   ("GPON OCAL Dev:"
					   " Optic Dev Open - <%s> found."
					   DTI_CRLF, buf));

			break;
		}
	}

	if (dev_fd < 0) {
		/* /dev/gpon */
		(void)DTI_snprintf(buf, 128, "%s", dev_name);
		dev_fd = DTI_DeviceOpen(buf);
	}

	if (dev_fd < 0) {
		DTI_PRN_USR_ERR_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_ERR,
				   ("WRN: GPON OCAL Dev:"
				   " Optic Dev Open - <%s> can't "
				   "open device, giving up."DTI_CRLF, buf));

		return IFX_ERROR;
	} else {
		DTI_PRN_USR_DBG_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_HIGH,
				   ("GPON OCAL Dev:"
				   " Optic Dev Open - <%s> found."
				   DTI_CRLF, buf));
	}

	return dev_fd;
}

/**
   Close a control device of the GPON Optic driver

\param
   dev_fd    - a valid device fd

\return
   If success IFX_SUCCESS else IFX_ERROR.

*/
DTI_STATIC IFX_int_t dti_ocal_dev_close(IFX_int_t dev_fd)
{
	IFX_int_t ret_val = IFX_ERROR;

	if (dev_fd > 0)
		ret_val = DTI_DeviceClose(dev_fd);

	return ret_val;
}

/*
   Check if a control port for the given line is already open,
   if not open the control port.

\param
   dti_ocal_ctx   - points to the GPON OCAL Device context.
\param
   line_num       - line number
\param
   packet_err     - returns the result

*/
DTI_STATIC IFX_int_t dti_ocal_cntrl_port_open(struct dti_dev_gpon_ocal_ctx
					     *dti_ocal_ctx,
					     IFX_int_t dev_num,
					     DTI_PacketError_t *packet_err)
{
	if (dti_ocal_ctx->p_optic_dev_fds[dev_num] < 0) {
		dti_ocal_ctx->p_optic_dev_fds[dev_num] =
		    dti_ocal_dev_open(DRV_IO_GPON_OPTIC);
		if (dti_ocal_ctx->p_optic_dev_fds[dev_num] < 0) {
			*packet_err = DTI_eErrPortOpen;

			DTI_PRN_USR_ERR_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_ERR,
					   ("ERROR: GPON OCAL Dev:"
					   " Optic cntrl port open "
					   "- failed, dev_num = %d."DTI_CRLF,
					   dev_num));

			return IFX_ERROR;
		}

		/** \todo Check if it is required */
		/* add the new device to the FDSET struct */
		IFXOS_DevFdSet(
			(IFX_uint32_t) dti_ocal_ctx->p_optic_dev_fds[dev_num],
			&dti_ocal_ctx->recv_dev_fds);

		if ((dti_ocal_ctx->p_optic_dev_fds[dev_num] + 1) >
		    dti_ocal_ctx->recv_max_dev_fd) {
			dti_ocal_ctx->recv_max_dev_fd =
			    dti_ocal_ctx->p_optic_dev_fds[dev_num] + 1;
		}
	}

	return IFX_SUCCESS;
}

/*
   Check if the corresponding debug control device for the given line is already
   open, if yes close the debug control device.

\param
   dti_ocal_ctx   - points to the GPON OCAL Device context.
\param
   dev_num        - line number
\param
   packet_err     - returns the result

*/
DTI_STATIC IFX_int_t dti_ocal_cntrl_port_close(struct dti_dev_gpon_ocal_ctx
					      *dti_ocal_ctx,
					      IFX_int_t dev_num,
					      DTI_PacketError_t *packet_err)
{
	if (dti_ocal_ctx->p_optic_dev_fds[dev_num] >= 0) {
		/* remove this device form the FDSET struct */
		dti_ocal_dev_fd_clear(dti_ocal_ctx, dev_num);

		(void)dti_ocal_dev_close(
				dti_ocal_ctx->p_optic_dev_fds[dev_num]);
		dti_ocal_ctx->p_optic_dev_fds[dev_num] = -1;
	}

	return IFX_SUCCESS;
}

/**
   Clear a device FD from the FD_SET and recalc new max FD.

*/
DTI_STATIC IFX_void_t dti_ocal_dev_fd_clear(struct dti_dev_gpon_ocal_ctx
					     *dti_ocal_ctx,
					     IFX_int_t dev_num)
{
#if defined(IFXOS_HAVE_DEV_FD_CLEAR)
	IFX_int_t i, max_dev_fd = 0;

	DTI_DevFdClear(dti_ocal_ctx->pDevFd[dev_num],
		       &dti_ocal_ctx->recv_dev_fds);

	for (i = 0; i < dti_ocal_ctx->num_of_devs; i++) {
		if (dti_ocal_ctx->p_optic_dev_fds[i] >= 0) {
			if ((DTI_DevFdIsSet(dti_ocal_ctx->p_optic_dev_fds[i],
					    &dti_ocal_ctx->recv_dev_fds)) &&
			    (dti_ocal_ctx->p_optic_dev_fds[i] > max_dev_fd)} {
				max_dev_fd = dti_ocal_ctx->p_optic_dev_fds[i];
		    }
		}
	}

	dti_ocal_ctx->recv_max_dev_fd = max_dev_fd + 1;
#else
	IFX_int_t i, max_dev_fd = 0;

	DTI_DevFdZero(&dti_ocal_ctx->tmp_dev_fds);
	dti_ocal_ctx->recv_max_dev_fd = 0;

	for (i = 0; i < dti_ocal_ctx->num_of_optic_devs; i++) {
		if ((dti_ocal_ctx->p_optic_dev_fds[i] >= 0) && (i != dev_num)) {
			if (DTI_DevFdIsSet((IFX_uint32_t) dti_ocal_ctx->
					   p_optic_dev_fds[i],
					   &dti_ocal_ctx->recv_dev_fds)) {
				DTI_DevFdSet((IFX_uint32_t) dti_ocal_ctx->
					     p_optic_dev_fds[i],
					     &dti_ocal_ctx->tmp_dev_fds);

				if (dti_ocal_ctx->p_optic_dev_fds[i]
								> max_dev_fd) {
					max_dev_fd =
						dti_ocal_ctx->
							p_optic_dev_fds[i];
				}
			}
		}
	}

	DTI_MemCpy(&dti_ocal_ctx->recv_dev_fds, &dti_ocal_ctx->tmp_dev_fds,
		   sizeof(IFXOS_devFd_set_t));
	dti_ocal_ctx->recv_max_dev_fd = (max_dev_fd) ? (max_dev_fd + 1) : 0;

	DTI_DevFdZero(&dti_ocal_ctx->tmp_dev_fds);

#endif
	return;
}

/*
   Request and return the device driver config.

\param
   sys_info       - return DTI_DeviceSysInfo_t structure.
\param
   pPortsPerDev   - return ptr, ports per device.

\return
   IFX_SUCCESS if request was successful, else
   IFX_ERROR
*/
DTI_STATIC IFX_int_t dti_ocal_dev_cfg_get(DTI_DeviceSysInfo_t *sys_info)
{
	sys_info->bControlAutoDevMsgSupport = IFX_FALSE;

	sys_info->numOfDevs = 1;
	sys_info->portsPerDev = 1;
	sys_info->ifPerDev = 1;

	if ((sys_info->numOfDevs > 0) && (sys_info->portsPerDev > 0)) {
		sys_info->numOfPorts =
		    sys_info->numOfDevs * sys_info->portsPerDev;
		sys_info->bValid = IFX_TRUE;
	}

	return IFX_SUCCESS;
}

/* ============================================================================
   Device Access Functions
   ========================================================================= */

/**
   Set a new debug level.
*/
DTI_STATIC IFX_int_t dti_ocal_printout_level_set(IFX_int_t new_dbg_level)
{

	IFXOS_PRN_USR_LEVEL_SET(DTI_GPON_OCAL, new_dbg_level);

	return IFX_SUCCESS;
}

/**
   Setup GPON OCAL Module

\param
   dev_sys_info - points to the system infos
\param
   dti_dev_ctx_out - pointer to return the allocated GPON OCAL Module struct.

\return
   If success IFX_SUCCESS, the context is returned via the dti_dev_ctx_out param
   else IFX_ERROR.

*/
DTI_STATIC IFX_int_t dti_ocal_module_setup(DTI_DeviceSysInfo_t *dev_sys_info,
					  DTI_DeviceCtx_t **dti_dev_ctx_out)
{
	IFX_int_t i, ctxSize = 0;
	DTI_DeviceCtx_t *dti_dev_ctx = IFX_NULL;
	struct dti_dev_gpon_ocal_ctx *ocal_dev_ctx = IFX_NULL;
	DTI_PTR_U u_dti_dev_ctx;

	if ((dti_dev_ctx_out == IFX_NULL) || (dev_sys_info == IFX_NULL)) {
		DTI_PRN_USR_ERR_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_ERR,
				   ("ERROR: GPON OCAL Dev Module Setup - NULL "
				    "ptr args."DTI_CRLF));

		return IFX_ERROR;
	}

	if (*dti_dev_ctx_out != IFX_NULL) {
		dti_dev_ctx = *dti_dev_ctx_out;
		ocal_dev_ctx = (struct dti_dev_gpon_ocal_ctx *)
							   dti_dev_ctx->pDevice;
	}

	if (dti_dev_ctx != IFX_NULL) {
		DTI_PRN_USR_ERR_NL(DTI_GPON_OCAL, IFXOS_PRN_LEVEL_WRN,
				   ("WARNING: GPON OCAL Dev Module Setup - "
				    "already done ."DTI_CRLF));

		return IFX_ERROR;
	}

	if (dti_ocal_dev_cfg_get(dev_sys_info) != IFX_SUCCESS) {
		DTI_PRN_USR_ERR_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_ERR,
				   ("Error: GPON OCAL Dev Module Setup - "
				    "driver config."DTI_CRLF));

		return IFX_ERROR;
	}

	DTI_PRN_USR_DBG_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_HIGH,
			   ("GPON OCAL Dev Module Setup - update driver config "
			    "(devs = %d, portsPerDev = %d)."
			    DTI_CRLF, dev_sys_info->numOfDevs,
			    dev_sys_info->portsPerDev));

	ctxSize = sizeof(DTI_DeviceCtx_t) +
			  sizeof(struct dti_dev_gpon_ocal_ctx) +
			  (sizeof(IFX_int_t) * dev_sys_info->numOfDevs);

	ctxSize +=
	    ((dev_sys_info->bControlAutoDevMsgSupport ==
	      IFX_TRUE) ? (DTI_DEV_GPON_OCAL_DEFAULT_MBOX_SIZE) : 0);

	u_dti_dev_ctx.pUInt8 = DTI_Malloc((IFX_uint_t) ctxSize);

	if (u_dti_dev_ctx.pUInt8 == IFX_NULL) {
		DTI_PRN_USR_ERR_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_ERR,
				   ("ERROR: GPON OCAL Dev Module Setup - "
				    "Dev Struct alloc."DTI_CRLF));

		return IFX_ERROR;
	}
	DTI_MemSet(u_dti_dev_ctx.pUInt8, 0x0, ctxSize);

	/* set context pointer */
	dti_dev_ctx = (DTI_DeviceCtx_t *) DTI_PTR_CAST_GET_ULONG(u_dti_dev_ctx);
	if (dti_dev_ctx == IFX_NULL) {
		DTI_Free(u_dti_dev_ctx.pUInt8);

		DTI_PRN_USR_ERR_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_ERR,
				   ("ERROR: GPON OCAL Dev Module Setup - Dev "
				    "Struct miss-aligned."DTI_CRLF));

		return IFX_ERROR;
	}

	/* set pDevice pointer */
	u_dti_dev_ctx.pUInt8 += sizeof(DTI_DeviceCtx_t);
	ocal_dev_ctx = (struct dti_dev_gpon_ocal_ctx *)
					    DTI_PTR_CAST_GET_UINT32(
								u_dti_dev_ctx);
	if (ocal_dev_ctx == IFX_NULL) {
		DTI_Free(dti_dev_ctx);

		DTI_PRN_USR_ERR_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_ERR,
				   ("ERROR: GPON OCAL Dev Module Setup - "
				    "pDevice miss-aligned."DTI_CRLF));

		return IFX_ERROR;
	}

	/* set dev_fd pointer */
	u_dti_dev_ctx.pUInt8 += sizeof(struct dti_dev_gpon_ocal_ctx);
	ocal_dev_ctx->p_optic_dev_fds = (IFX_int_t *)
					    DTI_PTR_CAST_GET_UINT32(
								u_dti_dev_ctx);

	if (ocal_dev_ctx->p_optic_dev_fds == IFX_NULL) {
		DTI_Free(dti_dev_ctx);

		DTI_PRN_USR_ERR_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_ERR,
				   ("ERROR: GPON OCAL Dev Module Setup"
				   " - devFds miss-aligned."DTI_CRLF));

		return IFX_ERROR;
	}

	/* set auto msg buffer pointer */
	if (dev_sys_info->bControlAutoDevMsgSupport == IFX_TRUE) {
		u_dti_dev_ctx.pUInt8 +=
		    (sizeof(IFX_int_t) * dev_sys_info->numOfDevs);
		ocal_dev_ctx->recv_msg_buf =
		    (IFX_ulong_t *) DTI_PTR_CAST_GET_ULONG(u_dti_dev_ctx);

		if (ocal_dev_ctx->recv_msg_buf == IFX_NULL) {
			DTI_Free(dti_dev_ctx);

			DTI_PRN_USR_ERR_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_ERR,
					   ("ERROR: GPON OCAL Module Setup - "
					    "Gmx Auto MSg Buffer miss-aligned."
					    DTI_CRLF));

			return IFX_ERROR;
		}
		ocal_dev_ctx->recv_msg_buf_sz_byte =
		    DTI_DEV_GPON_OCAL_DEFAULT_MBOX_SIZE;
	} else {
		ocal_dev_ctx->recv_msg_buf = IFX_NULL;
		ocal_dev_ctx->recv_msg_buf_sz_byte = 0;
	}

	for (i = 0; i < dev_sys_info->numOfDevs; i++) {
		ocal_dev_ctx->p_optic_dev_fds[i] = -1;
	}

	ocal_dev_ctx->num_of_reg_access =
				DTI_DEV_GPON_OCAL_MAX_REGISTER_ACCESS_NUM;
	ocal_dev_ctx->num_of_dbg_access =
				DTI_DEV_GPON_OCAL_MAX_DEBUG_ACCESS_NUM;

	ocal_dev_ctx->num_of_optic_devs = dev_sys_info->numOfDevs;
	ocal_dev_ctx->if_per_optic_dev = dev_sys_info->ifPerDev;
	ocal_dev_ctx->recv_dev_msg_support =
	    dev_sys_info->bControlAutoDevMsgSupport;

	dti_dev_ctx->bAutoDevMsgActive = IFX_FALSE;
	dti_dev_ctx->pDevice = (IFX_void_t *) ocal_dev_ctx;
	*dti_dev_ctx_out = dti_dev_ctx;

	return IFX_SUCCESS;
}

/**
   Delete a GPON OCAL Module.
   - close all open devices
   - free memory

\param
   dev_sys_info - points to the system infos
\param
   dti_dev_ctx_out - contains the module context.

\return
   If success IFX_SUCCESS, the context is freed and the ptr is set to IFX_NULL.
   else IFX_ERROR.

*/
DTI_STATIC IFX_int_t dti_ocal_module_delete(DTI_DeviceSysInfo_t *dev_sys_info,
					   DTI_DeviceCtx_t **dti_dev_ctx_out)
{
	IFX_int_t i;
	DTI_DeviceCtx_t *dti_dev_ctx = IFX_NULL;
	struct dti_dev_gpon_ocal_ctx *dti_ocal_ctx = IFX_NULL;

	if (dti_dev_ctx_out == IFX_NULL) {
		DTI_PRN_USR_ERR_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_ERR,
				   ("ERROR: GPON OCAL Dev Module Delete - NULL "
				    "ptr args."DTI_CRLF));

		return IFX_ERROR;
	}

	if (*dti_dev_ctx_out == IFX_NULL) {
		DTI_PRN_USR_ERR_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_ERR,
				   ("ERROR: GPON OCAL Dev Module Delete - NULL "
				    "ptr modul ctx."DTI_CRLF));

		return IFX_ERROR;
	}

	dti_dev_ctx = *dti_dev_ctx_out;
	*dti_dev_ctx_out = IFX_NULL;

	dti_ocal_ctx = (struct dti_dev_gpon_ocal_ctx *)
							dti_dev_ctx->pDevice;
	for (i = 0; i < dti_ocal_ctx->num_of_optic_devs; i++) {
		if (dti_ocal_ctx->p_optic_dev_fds[i] >= 0) {
			(void)dti_ocal_dev_close(dti_ocal_ctx->
							p_optic_dev_fds[i]);
			dti_ocal_ctx->p_optic_dev_fds[i] = -1;
		}
	}

	DTI_Free(dti_dev_ctx);
	return IFX_SUCCESS;
}

/**
   Write the Sytem Info of the current DTI Agent instance to the given buffer.

\param
   pAgentSysInfo     - points to Sytem Info struct of the current agent
		       instance.
\param
   sys_info_buf    - points to the Sytem Info char buffer.
\param
   buffer_size        - buffer size.

\return
   Number of written bytes.
*/
DTI_STATIC IFX_int_t dti_ocal_sys_info_write(DTI_DeviceSysInfo_t *dev_sys_info,
					    IFX_char_t *sys_info_buf,
					    IFX_int_t buffer_size)
{
	IFX_int_t written_len = 0;
	const IFX_char_t *board_name = DTI_BOARD_NAME_STR_GPON_OCAL;

	(void)DTI_snprintf(&sys_info_buf[written_len],
			   buffer_size - written_len,
			   "VendorName=%s", DTI_VENDOR_NAME_STR);
	written_len = (IFX_int_t) DTI_StrLen(&sys_info_buf[written_len]) + 1;

	if (dev_sys_info->bValid == IFX_TRUE) {
		board_name = DTI_BOARD_NAME_STR_GPON_OCAL;
	}

	(void)DTI_snprintf(&sys_info_buf[written_len],
			   buffer_size - written_len,
			   "BoardName=%s", board_name);
	written_len += (IFX_int_t) DTI_StrLen(&sys_info_buf[written_len]) + 1;

	(void)DTI_snprintf(&sys_info_buf[written_len],
			   buffer_size - written_len,
			   "BoardRevision=0.0");
	written_len += (IFX_int_t) DTI_StrLen(&sys_info_buf[written_len]) + 1;

	if (dev_sys_info->bValid == IFX_TRUE) {
		(void)DTI_snprintf(&sys_info_buf[written_len],
				   buffer_size - written_len, "NumOfDevices=%d",
				   dev_sys_info->numOfDevs);
		written_len +=
		    (IFX_int_t) DTI_StrLen(&sys_info_buf[written_len]) + 1;

		(void)DTI_snprintf(&sys_info_buf[written_len],
				   buffer_size - written_len, "MaxChannel=%d",
				   dev_sys_info->numOfPorts);
		written_len +=
		    (IFX_int_t) DTI_StrLen(&sys_info_buf[written_len]) + 1;
	} else {
		(void)DTI_snprintf(&sys_info_buf[written_len],
				   buffer_size - written_len,
				   "NumOfDevices=<na>");
		written_len +=
		    (IFX_int_t) DTI_StrLen(&sys_info_buf[written_len]) + 1;

		(void)DTI_snprintf(&sys_info_buf[written_len],
				   buffer_size - written_len,
				   "MaxChannel=<na>");
		written_len +=
		    (IFX_int_t) DTI_StrLen(&sys_info_buf[written_len]) + 1;
	}

	return written_len;
}

/**
   Do an device reset on the selected device.

\param
   dti_dev_ctx      - points to the GPON OCAL Device context.
\param
   in_dev_reset     - points to the DTI Host2Dev Reset struct.
\param
   out_dev_reset    - points to the DTI Dev2Host Reset struct.
\param
   rst_mask_size_32 - number of mask elements (32 Bit).
\param
   packet_err       - return ptr, return the DTI Packet Error.

\return
   IFX_SUCCESS and returns the DTI Packet Error code.
   IFX_ERROR   currently no (only in case of non-DTI related errors).
*/
DTI_STATIC IFX_int_t dti_ocal_reset(DTI_DeviceCtx_t *dti_dev_ctx,
				   DTI_H2D_DeviceReset_t *in_dev_reset,
				   DTI_D2H_DeviceReset_t *out_dev_reset,
				   IFX_int_t rst_mask_size_32,
				   DTI_PacketError_t *packet_err)
{
	DTI_PRN_USR_DBG_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_HIGH,
			   ("WARNING: GPON OCAL Dev Reset"
			   " - not implemented." DTI_CRLF));

	*packet_err = DTI_eErrUnknown;

	return IFX_SUCCESS;
}

/**
   Do a FW download on the selected devices

\param
   dti_dev_ctx        - points to the GPON OCAL Device context.
\param
   dti_prot_srv_ctx   - points to the DTI protocol server context.
\param
   in_dev_dwnld       - points to the DTI Host2Dev Download struct.
\param
   out_dev_dwnld      - points to the DTI Dev2Host Download struct.
\param
   packet_err         - return ptr, return the DTI Packet Error.

\return
   IFX_SUCCESS and returns the DTI Packet Error code.
   IFX_ERROR   currently no (only in case of non-DTI related errors).
*/
DTI_STATIC IFX_int_t dti_ocal_download(DTI_DeviceCtx_t *dti_dev_ctx,
				      DTI_ProtocolServerCtx_t *dti_prot_srv_ctx,
				      DTI_H2D_DeviceDownload_t *in_dev_dwnld,
				      DTI_D2H_DeviceDownload_t *out_dev_dwnld,
				      DTI_PacketError_t *packet_err)
{
	DTI_PRN_USR_DBG_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_HIGH,
			   ("WARNING: GPON OCAL Dev Download"
			   "- not implemented." DTI_CRLF));

	*packet_err = DTI_eErrUnknown;

	return IFX_SUCCESS;
}

/**
   Open a given line device.

\param
   dti_dev_ctx     - points to the GPON OCAL Device context.
\param
   line_num        - line number.
\param
   packet_err      - return ptr, return the DTI Packet Error.

\return
   IFX_SUCCESS and returns the DTI Packet Error code.
   IFX_ERROR   currently no (only in case of non-DTI related errors).
*/
DTI_STATIC IFX_int_t dti_ocal_device_open(DTI_DeviceCtx_t *dti_dev_ctx,
					 IFX_int_t dev_num,
					 DTI_PacketError_t *packet_err)
{
	DTI_PRN_USR_DBG_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_HIGH,
			   ("WARNING: GPON OCAL Dev DeviceOpen"
			   " - not implemented." DTI_CRLF));

	*packet_err = DTI_eErrUnknown;

	return IFX_SUCCESS;
}

/**
   Close a given line device.

\param
   dti_dev_ctx     - points to the GPON OCAL Device context.
\param
   line_num        - line number.
\param
   packet_err      - return ptr, return the DTI Packet Error.

\return
   IFX_SUCCESS and returns the DTI Packet Error code.
   IFX_ERROR   currently no (only in case of non-DTI related errors).
*/
DTI_STATIC IFX_int_t dti_ocal_device_close(DTI_DeviceCtx_t *dti_dev_ctx,
					  IFX_int_t line_num,
					  DTI_PacketError_t *packet_err)
{
	DTI_PRN_USR_DBG_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_HIGH,
			   ("WARNING: GPON OCAL Dev DeviceClose"
			   " - not implemented." DTI_CRLF));

	*packet_err = DTI_eErrUnknown;

	return IFX_SUCCESS;
}

/**
   Get / release the device lock.

\param
   dti_dev_ctx     - points to the GPON OCAL Device context.
\param
   in_lock         - points to the DTI Host2Dev Lock struct.
\param
   out_lock        - points to the DTI Dev2Host Lock struct.
\param
   line_num        - line number.
\param
   packet_err   - return ptr, return the DTI Packet Error.

\return
   IFX_SUCCESS and returns the DTI Packet Error code.
   IFX_ERROR   currently no (only in case of non-DTI related errors).
*/
DTI_STATIC IFX_int_t dti_ocal_register_lock(DTI_DeviceCtx_t *dti_dev_ctx,
					   DTI_H2D_DeviceLock_t *in_lock,
					   DTI_D2H_DeviceLock_t *out_lock,
					   IFX_int_t line_num,
					   DTI_PacketError_t *packet_err)
{
	DTI_PRN_USR_DBG_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_HIGH,
			   ("WARNING: GPON OCAL Dev RegisterLock"
			   " - not implemented." DTI_CRLF));

	*packet_err = DTI_eErrUnknown;

	return IFX_SUCCESS;
}

/**
   Get a device register (MEI interface).

\param
   dti_dev_ctx        - points to the GPON OCAL Device context.
\param
   in_reg_get         - points to the DTI Host2Dev RegisterGet struct.
\param
   out_reg_get        - points to the DTI Dev2Host RegisterGet struct.
\param
   line_num           - line number.
\param
   out_payl_sz_byte   - return ptr, return the size of the read registers.
                        0 in case of error
\param
   packet_err         - return ptr, return the DTI Packet Error.

\return
   IFX_SUCCESS and returns the DTI Packet Error code.
   IFX_ERROR   if the register read operation fails.
*/
DTI_STATIC IFX_int_t dti_ocal_register_get(DTI_DeviceCtx_t *dti_dev_ctx,
					  DTI_H2D_RegisterGet_t *in_reg_get,
					  DTI_D2H_RegisterGet_t *out_reg_get,
					  IFX_int_t dev_num,
					  IFX_uint32_t *out_payl_sz_byte,
					  DTI_PacketError_t *packet_err)
{
	DTI_PRN_USR_DBG_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_HIGH,
			   ("WARNING: GPON OCAL Dev RegisterGet"
			   " - not implemented." DTI_CRLF));

	*packet_err = DTI_eErrUnknown;

	return IFX_SUCCESS;
}

/**
   Get a device register (MEI interface).

\param
   dti_dev_ctx     - points to the GPON OCAL Device context.
\param
   in_lock         - points to the DTI Host2Dev RegisterGet struct.
\param
   out_lock        - points to the DTI Dev2Host RegisterGet struct.
\param
   line_num        - line number.
\param
   packet_err      - return ptr, return the DTI Packet Error.

\return
   IFX_SUCCESS DTI Packet Error code is "no error".
   IFX_ERROR   if the register read operation fails.
*/
DTI_STATIC IFX_int_t dti_ocal_register_set(DTI_DeviceCtx_t *dti_dev_ctx,
					  DTI_H2D_RegisterSet_t *in_reg_set,
					  IFX_int_t in_reg_set_sz_byte,
					  IFX_int_t dev_num,
					  DTI_PacketError_t *packet_err)
{
	DTI_PRN_USR_DBG_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_HIGH,
			   ("WARNING: GPON OCAL Dev RegisterSet"
			   " - not implemented." DTI_CRLF));

	*packet_err = DTI_eErrUnknown;

	return IFX_SUCCESS;
}

/**
   Set a device configuration.

\param
   dti_dev_ctx     - points to the GPON OCAL Device context.
\param
   in_cfg_set      - points to the DTI Host2Dev ConfigSet struct.
\param
   out_cfg_set     - points to the DTI Dev2Host ConfigSet struct.
\param
   line_num        - line number.
\param
   packet_err      - return ptr, return the DTI Packet Error.

\return
   IFX_SUCCESS and returns the DTI Packet Error code.
   IFX_ERROR   currently no (only in case of non-DTI related errors).

\remark
   Changes of the autonomous message handling may have influence to the
   connection settings (select wait time)
*/
DTI_STATIC IFX_int_t dti_ocal_cfg_set(DTI_DeviceCtx_t *dti_dev_ctx,
					DTI_H2D_DeviceConfigSet_t *in_cfg_set,
					DTI_D2H_DeviceConfigSet_t *out_cfg_set,
					IFX_int_t line_num,
					DTI_PacketError_t *packet_err)
{
	DTI_PRN_USR_DBG_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_HIGH,
			   ("WARNING: GPON OCAL Dev CfgSet"
			   " - not implemented." DTI_CRLF));

	*packet_err = DTI_eErrUnknown;

	return IFX_SUCCESS;
}

/**
   Get the device configuration of the given line.

\param
   dti_dev_ctx     - points to the GPON OCAL Device context.
\param
   in_cfg_set      - points to the DTI Host2Dev ConfigSet struct.
\param
   out_cfg_set     - points to the DTI Dev2Host ConfigSet struct.
\param
   line_num        - line number.
\param
   packet_err      - return ptr, return the DTI Packet Error.

\return
   IFX_SUCCESS and returns the DTI Packet Error code.
   IFX_ERROR   currently no (only in case of non-DTI related errors).
*/
DTI_STATIC IFX_int_t dti_ocal_cfg_get(DTI_DeviceCtx_t *dti_dev_ctx,
					DTI_H2D_DeviceConfigGet_t *in_cfg_get,
					DTI_D2H_DeviceConfigGet_t *out_cfg_get,
					IFX_int_t line_num,
					DTI_PacketError_t *packet_err)
{
	DTI_PRN_USR_DBG_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_HIGH,
			   ("WARNING: GPON OCAL Dev CfgGet"
			   " - not implemented." DTI_CRLF));

	*packet_err = DTI_eErrUnknown;

	return IFX_SUCCESS;
}

/**
   Send a 32 Bit message to the device and wait for the responce.

\param
   dti_dev_ctx        - points to the GPON OCAL Device context.
\param
   in_msg32_send      - points to the DTI 32 bit message send struct.
\param
   out_msg32_send     - points to the DTI 32 bit message responce struct.
\param
   line_num           - line number.
\param
   in_payl_sz_byte    - payload size of the DTI packet [byte].
\param
   out_payl_sz_byte   - return ptr, return the OUT payload size.
\param
   packet_err         - return ptr, return the DTI Packet Error.

\return
   IFX_SUCCESS and returns the DTI Packet Error code.
   IFX_ERROR   currently no (only in case of non-DTI related errors).
*/
DTI_STATIC IFX_int_t dti_ocal_msg32_send(DTI_DeviceCtx_t *dti_dev_ctx,
					    DTI_H2D_Message32_u *in_msg32_send,
					    DTI_D2H_Message32_u *out_msg32_send,
					    IFX_int_t dev_num,
					    IFX_int_t in_payl_sz_byte,
					    IFX_int_t *out_payl_sz_byte,
					    DTI_PacketError_t *packet_err)
{
	DTI_PRN_USR_DBG_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_HIGH,
			   ("WARNING: GPON OCAL Dev Msg32Send"
			   " - not implemented." DTI_CRLF));

	*packet_err = DTI_eErrUnknown;

	return IFX_SUCCESS;
}

/**
   Send a 16 Bit message to the device.

\param
   dti_dev_ctx        - points to the GPON OCAL Device context.
\param
   in_msg16_send      - points to the DTI 16 bit message send struct.
\param
   out_msg16_send     - points to the DTI 16 bit message responce struct.
\param
   line_num           - line number.
\param
   in_payl_sz_byte    - payload size of the DTI packet [byte].
\param
   out_payl_sz_byte   - return ptr, return the OUT payload size [byte].
\param
   packet_err         - return ptr, return the DTI Packet Error.

\return
   IFX_SUCCESS and returns the DTI Packet Error code.
   IFX_ERROR   currently no (only in case of non-DTI related errors).
*/
DTI_STATIC IFX_int_t dti_ocal_msg16_send(DTI_DeviceCtx_t * dti_dev_ctx,
					    DTI_H2D_Message16_u *in_msg16_send,
					    DTI_D2H_Message16_u *out_msg16_send,
					    IFX_int_t line_num,
					    IFX_int_t in_payl_sz_byte,
					    IFX_int_t *out_payl_sz_byte,
					    DTI_PacketError_t *packet_err)
{
	DTI_PRN_USR_DBG_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_HIGH,
			   ("WARNING: GPON OCAL Dev Msg16Send"
			   " - not implemented." DTI_CRLF));

	*packet_err = DTI_eErrUnknown;

	return IFX_SUCCESS;
}

/**
   Send a 8 Bit message to the device and wait for the responce.

\param
   dti_dev_ctx        - points to the GPON OCAL Device context.
\param
   in_msg8_send       - points to the DTI 8 bit message send struct.
\param
   out_msg8_send      - points to the DTI 8 bit message responce struct.
\param
   line_num           - line number.
\param
   in_payl_sz_byte    - payload size of the DTI packet [byte].
\param
   out_payl_sz_byte   - return ptr, return the OUT payload size.
\param
   packet_err         - return ptr, return the DTI Packet Error.

\return
   IFX_SUCCESS and returns the DTI Packet Error code.
   IFX_ERROR   currently no (only in case of non-DTI related errors).
*/
DTI_STATIC IFX_int_t dti_ocal_msg8_send(DTI_DeviceCtx_t *dti_dev_ctx,
					DTI_H2D_Message8_u *in_msg8_send,
					DTI_D2H_Message8_u *out_msg8_send,
					IFX_int_t dev_num,
					IFX_int_t in_payl_sz_byte,
					IFX_int_t *out_payl_sz_byte,
					DTI_PacketError_t *packet_err)
{
	struct dti_dev_gpon_ocal_ctx *dti_ocal_ctx =
	    (struct dti_dev_gpon_ocal_ctx *) dti_dev_ctx->pDevice;

	IFX_uint32_t cmd;
	IFX_uint32_t data_sz;

	IFX_int_t ret = 0;

	dev_num = 0;
	*packet_err = DTI_eErrUnknown;

	/* extract OCAL command ID and size */
	cmd = *(unsigned int *)&in_msg8_send->raw.data[0];
	data_sz = *(unsigned int *)&in_msg8_send->
					raw.data[1 * sizeof(unsigned int)];
	cmd= ntohl (cmd);
	data_sz = ntohl (data_sz);
	/* ensure that data_sz is valid */
	if ((IFX_uint32_t) data_sz > (IFX_uint32_t) in_payl_sz_byte) {
		out_msg8_send->raw.sendResult = -1;
		*packet_err = DTI_eNoError;
		return IFX_SUCCESS;
	}

	/* open Optic Driver */
	if (dti_ocal_cntrl_port_open(dti_ocal_ctx, dev_num, packet_err) !=
	    IFX_SUCCESS) {
		DTI_PRN_USR_ERR_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_ERR,
				   ("ERROR: GPON OCAL device message 8 send - "
				    "dev_num %d, open error."DTI_CRLF,
				    dev_num));

		return IFX_ERROR;
	}

	/* execute optic calibration command */
	ret = ocal_cmd_execute(dti_ocal_ctx->p_optic_dev_fds[dev_num],
			       (enum ocal_cmd) cmd,
			       (void*) &in_msg8_send->
			       raw.data[2 * sizeof(unsigned int)],
			       (void*) &out_msg8_send->raw.data[0]);

	if (ret < 0) {
		DTI_PRN_USR_ERR_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_ERR,
				   ("ERROR: executing OCAL command 0x%x, ret %d"
				    DTI_CRLF, cmd, ret));
		out_msg8_send->raw.sendResult = ret;
		*packet_err = DTI_eNoError;
		*out_payl_sz_byte = 1;
		return IFX_SUCCESS;
	}
	/* set outgoing packet size */
	*out_payl_sz_byte = ret + 4;
	/* result is OK */
	out_msg8_send->raw.sendResult = OCAL_STATUS_OK;
	*packet_err = DTI_eNoError;

	return IFX_SUCCESS;
}

/**
   Setup the trace buffer configuration (debug streams).

\param
   dti_dev_ctx            - points to the GPON OCAL Device context.
\param
   in_trace_config_set    - points to the DTI Host2Dev TraceConfigSet struct.
\param
   out_trace_config_set   - points to the DTI Dev2Host TraceConfigSet struct.
\param
   line_num               - line number.
\param
   packet_err             - return ptr, return the DTI Packet Error.

\return
   IFX_SUCCESS and returns the DTI Packet Error code.
   IFX_ERROR   debug stream setup failed.
*/
DTI_STATIC IFX_int_t dti_ocal_trace_buf_cfg_set(DTI_DeviceCtx_t
						*dti_dev_ctx,
						DTI_H2D_TraceConfigSet_t
						*in_trace_config_set,
						DTI_D2H_TraceConfigSet_t
						*out_trace_config_set,
						IFX_int_t line_num,
						DTI_PacketError_t
						*packet_err)
{
	DTI_PRN_USR_DBG_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_HIGH,
			   ("WARNING: GPON OCAL Dev TraceBufferCfgGet"
			   " - not implemented." DTI_CRLF));

	*packet_err = DTI_eErrUnknown;

	return IFX_SUCCESS;
}

/**
   Reset the current trace buffer (debug streams).

\param
   dti_dev_ctx     - points to the GPON OCAL Device context.
\param
   line_num        - line number.
\param
   packet_err      - return ptr, return the DTI Packet Error.

\return
   IFX_SUCCESS and returns the DTI Packet Error code.
   IFX_ERROR   debug stream setup failed.
*/
DTI_STATIC IFX_int_t dti_ocal_trace_buf_reset(DTI_DeviceCtx_t *dti_dev_ctx,
						IFX_int_t line_num,
						DTI_PacketError_t *packet_err)
{
	DTI_PRN_USR_DBG_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_HIGH,
			   ("WARNING: GPON OCAL Dev TraceBufferReset"
			   " - not implemented." DTI_CRLF));

	*packet_err = DTI_eErrUnknown;

	return IFX_SUCCESS;
}

/**
   Setup the trace buffer configuration (debug streams).

\param
   dti_dev_ctx            - points to the GPON OCAL Device context.
\param
   out_trace_status_get   - points to the DTI Dev2Host TraceStatusGet struct.
\param
   line_num               - line number.
\param
   packet_err             - return ptr, return the DTI Packet Error.

\return
   IFX_SUCCESS and returns the DTI Packet Error code.
   IFX_ERROR   debug stream config request failed.
*/
DTI_STATIC IFX_int_t dti_ocal_trace_buf_status_get(DTI_DeviceCtx_t
						     *dti_dev_ctx,
						     DTI_D2H_TraceStatusGet_t
						     *out_trace_status_get,
						     IFX_int_t line_num,
						     DTI_PacketError_t
						     *packet_err)
{
	DTI_PRN_USR_DBG_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_HIGH,
			   ("WARNING: GPON OCAL Dev TraceBufferStatusGet"
			   " - not implemented." DTI_CRLF));

	*packet_err = DTI_eErrUnknown;

	return IFX_SUCCESS;
}

/**
   Read data from the device via Debug Access (MEI Debug).

\param
   dti_dev_ctx           - points to the GPON OCAL Device context.
\param
   in_trace_buf_get      - points to the DTI Host2Dev RegisterGet struct.
\param
   used_dti_packet_out   - return ptr, points to the DTI Out packet.
\param
   used_buffer_out_size  - return value, points to the DTI Out packet size.
\param
   line_num              - line number.
\param
   tr_buf_read_sz_byte   - return ptr, return the number of read registers.
\param
   packet_err            - return ptr, return the DTI Packet Error.

\remark
   The function is called with the standard out packet. If the requested size
   does not fit into this buffer, a corresponding buffer is allocated and used
   for the ioctl call. The pointer to this out packet will be returned.

\return
   IFX_SUCCESS  debug stream data read successful.
      - returns the DTI Packet Error code.
      - pointer to the used out package.
      - size of the used out package.
   IFX_ERROR   data read failed.
*/
DTI_STATIC IFX_int_t dti_ocal_trace_buf_get(DTI_DeviceCtx_t *dti_dev_ctx,
					      DTI_H2D_TraceBufferGet_t
					      *in_trace_buf_get,
					      DTI_Packet_t
					      **used_dti_packet_out,
					      IFX_int_t *used_buffer_out_size,
					      IFX_int_t line_num,
					      IFX_int_t *tr_buf_read_sz_byte,
					      DTI_PacketError_t *packet_err)
{
	DTI_PRN_USR_DBG_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_HIGH,
			   ("WARNING: GPON OCAL Dev TraceBufferGet"
			   " - not implemented." DTI_CRLF));

	*packet_err = DTI_eErrUnknown;

	return IFX_SUCCESS;
}

/**
   Read data from the device via Debug Access (MEI Debug).

\param
   dti_dev_ctx     - points to the GPON OCAL Device context.
\param
   in_dbg_get      - points to the DTI Host2Dev RegisterGet struct.
\param
   out_dbg_get     - points to the DTI Dev2Host RegisterGet struct.
\param
   line_num        - line number.
\param
   dbg_read_count  - return ptr, return the number of read registers.
\param
   packet_err   - return ptr, return the DTI Packet Error.

\return
   IFX_SUCCESS and returns the DTI Packet Error code.
   IFX_ERROR   currently no (only in case of non-DTI related errors).
*/
DTI_STATIC IFX_int_t dti_ocal_debug_read(DTI_DeviceCtx_t *dti_dev_ctx,
					DTI_H2D_DebugRead_t *in_dbg_get,
					DTI_D2H_DebugRead_t *out_dbg_get,
					IFX_int_t line_num,
					IFX_int_t *dbg_read_count,
					DTI_PacketError_t *packet_err)
{
	DTI_PRN_USR_DBG_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_HIGH,
			   ("WARNING: GPON OCAL Dev DebugRead"
			   " - not implemented." DTI_CRLF));

	*dbg_read_count = 0;
	*packet_err = DTI_eErrUnknown;

	return IFX_SUCCESS;
}

/**
   Write data to the device via Debug Access (MEI Debug).

\param
   dti_dev_ctx     - points to the GPON OCAL Device context.
\param
   in_dbg_set      - points to the DTI Host2Dev RegisterGet struct.
\param
   out_dbg_get_nu  - not used.
\param
   line_num        - line number.
\param
   dbg_write_count - return ptr, return the number of read registers.
\param
   packet_err      - return ptr, return the DTI Packet Error.

\return
   IFX_SUCCESS and returns the DTI Packet Error code.
   IFX_ERROR   currently no (only in case of non-DTI related errors).
*/
DTI_STATIC IFX_int_t dti_ocal_debug_write(DTI_DeviceCtx_t *dti_dev_ctx,
					 DTI_H2D_DebugWrite_t *in_dbg_set,
					 IFX_uint32_t *out_dbg_get_nu,
					 IFX_int_t line_num,
					 IFX_int_t *dbg_write_count,
					 DTI_PacketError_t *packet_err)
{
	DTI_PRN_USR_DBG_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_HIGH,
			   ("WARNING: GPON OCAL Dev DebugRead"
			   " - not implemented." DTI_CRLF));

	*dbg_write_count = 0;
	*packet_err = DTI_eErrUnknown;

	return IFX_SUCCESS;
}

/**
   Check and process for Device Autonomous Messages.
   The Auto Msg is read form the device (driver) and a corresponding
   DTI packet is created and sent to the upper DTI Client

\param
   dti_dev_ctx           - points to the Device context.
\param
   dti_con               - points to the established DTI Connection data.
\param
   dev_select_wait_ms    - wait time [ms] for the device select operation.
\param
   out_buffer            - points to the DTI packet out buffer to create a DTI
			               packet.
\param
   out_buffer_sz_byte    - DTI out buffer size [byte]

\return
   IFX_SUCCESS if the DTI packet has been sent.
   IFX_ERROR   DTI packet has not been sent.

\remarks
   At first the device msg will be read out to free the internal driver buffer.
   This is done independant on the established connection.
*/
DTI_STATIC IFX_int_t dti_ocal_auto_msg_process(DTI_DeviceCtx_t *dti_dev_ctx,
					      const DTI_Connection_t *dti_con,
					      IFX_uint32_t dev_select_wait_ms,
					      IFX_char_t *out_buffer,
					      IFX_int_t out_buffer_sz_byte)
{
	DTI_PRN_USR_DBG_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_HIGH,
			   ("WARNING: GPON OCAL Dev AutoMsgProcess"
			   " - not implemented." DTI_CRLF));

	return IFX_SUCCESS;
}

DTI_STATIC IFX_int_t dti_ocal_win_easy_cli_access(DTI_DeviceCtx_t *dti_dev_ctx,
						 IFX_int_t line_num,
						 const IFX_uint8_t *data_in,
						 const IFX_uint32_t size_in,
						 IFX_uint8_t *data_out,
						 const IFX_uint32_t size_out,
						 DTI_PacketError_t *packet_err)
{
	DTI_PRN_USR_DBG_NL(DTI_GPON_OCAL, DTI_PRN_LEVEL_HIGH,
			   ("WARNING: GPON OCAL Dev WinEasyCiAccess"
			   " - not implemented." DTI_CRLF));

	*packet_err = DTI_eErrUnknown;
	return IFX_SUCCESS;
}

#endif				/* DEVICE_GPON_OCAL */
