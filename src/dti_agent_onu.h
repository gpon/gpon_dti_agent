#ifndef _DTI_AGENT_ONU_H
#define _DTI_AGENT_ONU_H
/******************************************************************************

                              Copyright (c) 2011
                            Lantiq Deutschland GmbH
                     Am Campeon 3; 85579 Neubiberg, Germany

  For licensing information, see the file 'LICENSE' in the root folder of
  this software module.

******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif

/** \file
   GPON ONU device, control struct for GPON Optic access and handling.
*/

/* ==========================================================================
   includes
   ========================================================================== */
#include "ifx_types.h"
#include "ifxos_debug.h"
#include "ifxos_device_access.h"

/* ==========================================================================
   defines
   ========================================================================== */

/** DTI Device Interface Name */
#define DTI_DEV_GPON_ONU_IF_NAME			"GPON-ONU"

#define DTI_BOARD_NAME_STR_GPON_ONU			"GPON Board (ONU)"

#define DTI_GPON_ONU_START_ID				0
#define DTI_GPON_ONU_MAX_ID				3

/** Max number of register accesses per packet */
#define DTI_DEV_GPON_ONU_MAX_REGISTER_ACCESS_NUM	0x10
/** Max number of debug accesses per packet */
#define DTI_DEV_GPON_ONU_MAX_DEBUG_ACCESS_NUM		0x10

/** default GPON ONU device mailbox size */
#define DTI_DEV_GPON_ONU_DEFAULT_MBOX_SIZE		0x100

/* ==========================================================================
   macro
   ========================================================================== */

/* ==========================================================================
   types
   ========================================================================== */

/* ==========================================================================
   exports
   ========================================================================== */

extern IFX_int_t dti_onu_cli_send(IFX_void_t *cb_ctx,
				       const IFX_char_t *cmd_in,
				       IFX_char_t *result_out,
				       IFX_int_t *result_sz_byte,
				       IFX_int_t *result_code);

#ifdef __cplusplus
}
#endif
#endif				/* _DTI_AGENT_ONU_H */
