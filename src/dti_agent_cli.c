/******************************************************************************

                              Copyright (c) 2010
                            Lantiq Deutschland GmbH
                     Am Campeon 3; 85579 Neubiberg, Germany

  For licensing information, see the file 'LICENSE' in the root folder of
  this software module.

******************************************************************************/

#include "dti_osmap.h"
#include "dti_agent_cli.h"

/**
   This function registers a corresponding send function within the DTI.
   The function is used to send incoming command strings to the caller
   command line interface (CLI).
   The send function must have the format of \ref DTI_CliSendCommandFunc_t

\param
   pDtiAgentContext  - points to the DTI Agent context.
\param
   dev_name    - GPON Optic device base name
\param
   cli_if_name           - CLI name, via this name the CLI will be identified from
                       the remote client.
\param
   send_fct          - function pointer to the send function to send a command
                       on this CLI.
                       Provide a NULL pointer to reserve the CLI interface and
                       to get a interface number (for example, only to register
                       an event call back).
\param
   resp_buf_sz - the buffer size required by the callback function.

\return
   In case of success, the CLI interface is marked as reserved and the
   interface number is returned (>= 0)
   In case of error IFX_ERROR will be return.

\remarks
   The last available interface is the default loop back interface.
*/
IFX_int_t dti_cli_add(DTI_AgentCtx_t *agent_ctx,
			     const IFX_char_t *dev_name,
			     const IFX_char_t *cli_if_name,
			     DTI_CliSendCommandFunc_t send_fct,
			     IFX_uint_t resp_buf_sz)
{
	struct dti_cli_ctx *cli_ctx = DTI_Malloc(sizeof(struct dti_cli_ctx));

	(void)DTI_snprintf(cli_ctx->dev_name, DTI_MAX_LEN_DEVICE_INTERFACE_NAME,
			   "%s", dev_name);

	cli_ctx->dev_fd = -1;

	cli_ctx->cli_interface = DTI_CLI_SendFunctionRegister(agent_ctx,
							      cli_ctx,
							      cli_if_name,
							      send_fct,
							      resp_buf_sz);
	return IFX_SUCCESS;
}
