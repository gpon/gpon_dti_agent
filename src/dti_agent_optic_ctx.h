/******************************************************************************

                              Copyright (c) 2011
                            Lantiq Deutschland GmbH
                     Am Campeon 3; 85579 Neubiberg, Germany

  For licensing information, see the file 'LICENSE' in the root folder of
  this software module.

******************************************************************************/
#ifndef _DTI_AGENT_OPTIC_CTX_H
#define _DTI_AGENT_OPTIC_CTX_H

#ifdef __cplusplus
extern "C" {
#endif

/** \file
   GPON Optic device context, control struct for GPON Optic access and handling.
*/

/* ==========================================================================
   includes
   ========================================================================== */
#include "ifx_types.h"
#include "ifxos_debug.h"
#include "ifxos_device_access.h"

/* ==========================================================================
   defines
   ========================================================================== */

/* ==========================================================================
   macro
   ========================================================================== */

/* ==========================================================================
   types
   ========================================================================== */

/**
   GPON Optic Driver Control Struct

   A GPON optic system is identified by its
   - number of devices

   To access a device via the GPON Optic driver the driver provides for each
   interface a device.

*/
struct dti_dev_gpon_optic_drv_ctx {
	/** number of devices */
	IFX_int_t num_of_devs;

	/** interfaces per device */
	IFX_int_t if_per_dev;

	/** points to the array of devices fds (number of devices * interfaces
	   per device) */
	IFX_int_t *p_dev_fds;

	/*
	   DTI Config
	 */
	/** max number of register requests / sets */
	IFX_int_t num_of_reg_access;
	/** max number of debug requests / sets */
	IFX_int_t num_of_dbg_access;

	/** recv msg handling - supported */
	IFX_boolean_t recv_dev_msg_support;
	/** receive msg handling - max device fd for select */
	IFX_int_t recv_max_dev_fd;
	/** receive msg handling - device FDSET */
	IFXOS_devFd_set_t recv_dev_fds;
	/** receive handling - for temporary use, device FDSET */
	IFXOS_devFd_set_t tmp_dev_fds;
	/** receive msg handling - recv buffer for upcoming msg's */
	IFX_ulong_t *recv_msg_buf;
	/** auto msg handling - size of the modem message buffer for NFC
	   handling */
	IFX_int_t recv_msg_buf_sz_byte;

};

/* ==========================================================================
   exports
   ========================================================================== */

#ifdef __cplusplus
}
#endif
#endif				/* _DTI_AGENT_OPTIC_CTX_H */
