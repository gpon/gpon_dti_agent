/******************************************************************************

                               Copyright (c) 2011
                            Lantiq Deutschland GmbH
                     Am Campeon 3; 85579 Neubiberg, Germany

  For licensing information, see the file 'LICENSE' in the root folder of
  this software module.

******************************************************************************/

#include "dti_osmap.h"
#include "dti_connection_interface.h"
#include "dti_agent_interface.h"
#include "dti_agent_main.h"
#include "dti_agent_cli.h"

#if defined(LINUX)
#  include <getopt.h>
#  define  DTI_GETOPT_DUMMY   NULL
#else
#  include "ifx_getopt.h"
#  define  DTI_GETOPT_DUMMY   0
#endif

#include "ifx_getopt_ext.h"

#if defined(DEVICE_GPON_ONU)
#  include "dti_agent_onu.h"
#  include "dti_agent_onu_ctx.h"
#  include "drv_onu_interface.h"
#endif

#if defined(DEVICE_GPON_OPTIC)
#  include "dti_agent_optic.h"
#  include "dti_agent_optic_ctx.h"
#  include "drv_optic_interface.h"
#endif

/* ==========================================================================
   Defines and types
   ========================================================================== */

#ifdef DTI_STATIC
#undef DTI_STATIC
#endif

#ifdef DTI_DEBUG
#define DTI_STATIC
#else
#define DTI_STATIC   static
#endif

/* for testing, scan for input to terminate */
#define DTI_TEST_MODE   1

/*
   Local arguments
*/
struct dti_op_arguments {
	GetOptExt_IntArg_t help;	/* h */
	GetOptExt_IntArg_t dbg_level_device;	/* v */
	GetOptExt_IntArg_t num_of_worker;	/* w */
	GetOptExt_IntArg_t run_in_forground;	/* f */
	GetOptExt_IntArg_t lines_per_device;	/* l */
	GetOptExt_IntArg_t devices;	/* d */
	GetOptExt_IntArg_t dti_port;	/* p */
	GetOptExt_IntArg_t dev_auto_msg;	/* D */
	GetOptExt_IntArg_t cli_auto_msg;	/* C */
};

#if defined(DEVICE_GPON_ONU)
extern DTI_DeviceAccessFct_t dti_dev_access_fct_gpon_onu;
#endif

#if defined(DEVICE_GPON_OPTIC)
extern DTI_DeviceAccessFct_t dti_dev_access_fct_gpon_optic;
#endif

#if defined(DEVICE_GPON_OCAL)
extern DTI_DeviceAccessFct_t dti_dev_access_fct_gpon_ocal;
#endif

#if defined(DEVICE_GENERIC)
extern DTI_DeviceAccessFct_t DTI_DeviceAccessFct_GENERIC;
#endif

/* ============================================================================
   Local defines
   ========================================================================= */

/* ==========================================================================
   Local Function Declaration
   ========================================================================== */
DTI_STATIC IFX_void_t dti_agent_do_help(void);

DTI_STATIC IFX_int_t dti_agent(DTI_AgentCtx_t **dti_agent_ctx_out);

/* ==========================================================================
   Local Variables
   ========================================================================== */

DTI_STATIC struct option dti_long_options[] = {
/*  0 */ {"Help", no_argument, DTI_GETOPT_DUMMY, 'h'},
/*  1 */ {"dev_dbg", required_argument, DTI_GETOPT_DUMMY, 'v'},
/*  2 */ {"num_wkr", required_argument, DTI_GETOPT_DUMMY, 'w'},
/*  3 */ {"fground", no_argument, DTI_GETOPT_DUMMY, 'f'},
/*  4 */ {"Lines", required_argument, DTI_GETOPT_DUMMY, 'l'},
/*  5 */ {"Devices", required_argument, DTI_GETOPT_DUMMY, 'd'},
/*  6 */ {"Port", required_argument, DTI_GETOPT_DUMMY, 'p'},
/*  7 */ {"DevAuto", required_argument, DTI_GETOPT_DUMMY, 'D'},
/*  8 */ {"CliAuto", required_argument, DTI_GETOPT_DUMMY, 'C'},
/*    */ {"ip_Addr", required_argument, DTI_GETOPT_DUMMY, 'a'},
	{NULL, 0, DTI_GETOPT_DUMMY, 0}
};

/*
                                   01 2 34 5   */
#define DTI_GETOPT_LONG_OPTSTRING "hv:w:fl:d:p:D:C:a:"

DTI_STATIC const char *DTI_description[] = {
/*  0 */ "help screen",
/*  1 */ "debug level device -v <new level>",
/*  2 */ "num of worker thr  -w <num of worker>",
/*  3 */ "run in forground   -f <none>",
/*  4 */ "lines per device   -l <lines>",
/*  5 */ "devices            -d <devices>",
/*  6 */ "server port        -p <port>",
/*  7 */ "device AutoMsg     -D 1",
/*  8 */ "CLI AutoMsg        -C 1",
/*    */ "server IP address  -a <ip addr (ASCII)>",
	IFX_NULL
};

/** getOpt option argument list */
DTI_STATIC struct dti_op_arguments dti_op_args;
/*
                                    { {0, IFX_FALSE} };
*/

/** getOpt string argument list */
DTI_STATIC GetOptExt_StrArg_t dti_str_params[GET_OPT_EXT_MAX_STR_PARAMS] =
    { {"\0", IFX_FALSE} };

#if defined(DTI_LIBRARY)

/** max argument string len */
#define DTI_OPTIONS_MAX_LEN      255
/** max number of arguments */
#define DTI_OPTIONS_MAX_NUM      32

/** keep a copy of the original argument string, each argument will be
    terminated */
DTI_STATIC IFX_char_t dti_control_arg_str[DTI_OPTIONS_MAX_LEN];
/** pointer array to point to the single arguments within the arg-string */
DTI_STATIC IFX_char_t *dti_control_options[DTI_OPTIONS_MAX_NUM] = { IFX_NULL };

#endif

/* ==========================================================================
   Global Variables
   ========================================================================== */
IFX_int_t dti_main_run = 0;

/* ==========================================================================
   Local Function Definitons
   ========================================================================== */

/**
   Printout the "Help Info" for the available arguments.
*/
DTI_STATIC IFX_void_t dti_agent_do_help(void)
{
	struct option *ptr;
	const char **desc = &DTI_description[0];
	ptr = dti_long_options;

	DTI_Printf("%s" DTI_CRLF, DTI_AGENT_WHAT_STR);
	DTI_Printf("usage: dti agent [options]" DTI_CRLF);
	DTI_Printf("following options defined:" DTI_CRLF);

	while (ptr->name) {
		DTI_Printf(" --%s \t(-%c) - %s" DTI_CRLF, ptr->name, ptr->val,
			   *desc);

		ptr++;
		desc++;
	}

	DTI_Printf(DTI_CRLF);

	return;
}

/*
   Start the DTI agent with the given startup parameters
*/
DTI_STATIC IFX_int_t dti_agent(DTI_AgentCtx_t **dti_agent_ctx_out)
{
	IFX_int_t dev_if_num = 0, num_of_devices = 0, lines_per_device =
	    0, auto_dev_msg = 0;
	DTI_AgentCtx_t *dti_agent_ctx;
	DTI_AgentStartupSettingsXDevs_t agent_atartup_settings_xdevs;
	DTI_DeviceAccessFct_t *dev_access_fct = IFX_NULL;

	DTI_MemSet(&agent_atartup_settings_xdevs, 0x00,
		   sizeof(DTI_AgentStartupSettingsXDevs_t));

	agent_atartup_settings_xdevs.debugLevel =
	    (dti_op_args.dbg_level_device.bSet ==
	     IFX_TRUE) ? dti_op_args.dbg_level_device.intValue : 4;

	agent_atartup_settings_xdevs.numOfUsedWorker =
	    (dti_op_args.num_of_worker.bSet ==
	     IFX_TRUE) ? dti_op_args.num_of_worker.intValue : 0;

	agent_atartup_settings_xdevs.bStartupAutoCliMsgSupport =
	    (dti_op_args.cli_auto_msg.bSet ==
	     IFX_TRUE) ? dti_op_args.cli_auto_msg.intValue : 0;

	agent_atartup_settings_xdevs.listenPort = (IFX_uint16_t)
	    ((dti_op_args.dti_port.bSet ==
	      IFX_TRUE) ? dti_op_args.dti_port.intValue : 0);

	if (dti_str_params[0].bSet == IFX_TRUE) {
		DTI_MemCpy(agent_atartup_settings_xdevs.serverIpAddr,
			   dti_str_params[0].strValue, 16);
		agent_atartup_settings_xdevs.serverIpAddr[15] = '\0';
	}

	num_of_devices =
	    (dti_op_args.devices.bSet ==
	     IFX_TRUE) ? dti_op_args.devices.intValue : 0;
	lines_per_device =
	    (dti_op_args.lines_per_device.bSet ==
	     IFX_TRUE) ? dti_op_args.lines_per_device.intValue : 0;
	auto_dev_msg =
	    (dti_op_args.dev_auto_msg.bSet ==
	     IFX_TRUE) ? dti_op_args.dev_auto_msg.intValue : 0;

#if STARTUP_WITH_REGISTERED_DEVICE
#if defined(DEVICE_GPON_ONU)
	if (dev_if_num < DTI_MAX_DEVICE_INTERFACES) {
		dev_access_fct = &dti_dev_access_fct_gpon_onu;
		if ( /*(dev_if_num > 0) && */ (dev_access_fct != IFX_NULL)) {
			/* workaround (for testing multiple devices)
			   this is not the first device, use default values for
			   setup */
			num_of_devices = 1;
			lines_per_device = 1;
			auto_dev_msg = 1;
		}

		if ((dev_access_fct != IFX_NULL) &&
		    (num_of_devices > 0) && (lines_per_device > 0)) {
			DTI_Printf("Device[%d]: Add GPON ONU - devs = %d "
				   "lines per devs = %d\n",
						dev_if_num, num_of_devices,
						lines_per_device);

			agent_atartup_settings_xdevs.devIfSettings[dev_if_num].
			    numOfDevices = num_of_devices;
			agent_atartup_settings_xdevs.devIfSettings[dev_if_num].
			    linesPerDevice = lines_per_device;
			agent_atartup_settings_xdevs.devIfSettings[dev_if_num].
			    pDeviceAccessFct = dev_access_fct;
			agent_atartup_settings_xdevs.devIfSettings[dev_if_num].
			    bStartupAutoDevMsgSupport = auto_dev_msg;

			dev_if_num++;
		}
		dev_access_fct = IFX_NULL;
	}
#endif

#if defined(DEVICE_GPON_OPTIC)
	if (dev_if_num < DTI_MAX_DEVICE_INTERFACES) {
		dev_access_fct = &dti_dev_access_fct_gpon_optic;
		if ( /*(dev_if_num > 0) && */ (dev_access_fct != IFX_NULL)) {
			/* workaround (for testing multiple devices)
			   this is not the first device, use default values for
			   setup */
			num_of_devices = 1;
			lines_per_device = 1;
			auto_dev_msg = 1;
		}

		if ((dev_access_fct != IFX_NULL) &&
		    (num_of_devices > 0) && (lines_per_device > 0)) {
			DTI_Printf
			    ("Device[%d]: Add GPON OPTIC - devs = %d "
			     "lines per devs = %d\n", dev_if_num,
						      num_of_devices,
						      lines_per_device);

			agent_atartup_settings_xdevs.devIfSettings[dev_if_num].
			    numOfDevices = num_of_devices;
			agent_atartup_settings_xdevs.devIfSettings[dev_if_num].
			    linesPerDevice = lines_per_device;
			agent_atartup_settings_xdevs.devIfSettings[dev_if_num].
			    pDeviceAccessFct = dev_access_fct;
			agent_atartup_settings_xdevs.devIfSettings[dev_if_num].
			    bStartupAutoDevMsgSupport = auto_dev_msg;

			dev_if_num++;

		}
		dev_access_fct = IFX_NULL;
	}
#endif

#if defined(DEVICE_GPON_OCAL)
	if (dev_if_num < DTI_MAX_DEVICE_INTERFACES) {
		dev_access_fct = &dti_dev_access_fct_gpon_ocal;
		if ( /*(dev_if_num > 0) && */ (dev_access_fct != IFX_NULL)) {
			/* workaround (for testing multiple devices)
			   this is not the first device, use default values for
			   setup */
			num_of_devices = 1;
			lines_per_device = 1;
			auto_dev_msg = 1;
		}

		if ((dev_access_fct != IFX_NULL) &&
		    (num_of_devices > 0) && (lines_per_device > 0)) {
			DTI_Printf
			    ("Device[%d]: Add GPON OCAL - devs = %d "
			     "lines per devs = %d\n", dev_if_num,
						      num_of_devices,
						      lines_per_device);

			agent_atartup_settings_xdevs.devIfSettings[dev_if_num].
			    numOfDevices = num_of_devices;
			agent_atartup_settings_xdevs.devIfSettings[dev_if_num].
			    linesPerDevice = lines_per_device;
			agent_atartup_settings_xdevs.devIfSettings[dev_if_num].
			    pDeviceAccessFct = dev_access_fct;
			agent_atartup_settings_xdevs.devIfSettings[dev_if_num].
			    bStartupAutoDevMsgSupport = auto_dev_msg;

			dev_if_num++;

		}
		dev_access_fct = IFX_NULL;
	}
#endif

#if defined(DEVICE_GENERIC)
	if (dev_if_num < DTI_MAX_DEVICE_INTERFACES) {
		dev_access_fct = &DTI_DeviceAccessFct_GENERIC;
		if ((dev_if_num > 0) && (dev_access_fct != IFX_NULL)) {
			/* workaround (for testing multiple devices)
			   this is not the first device, use default values for
			   setup */
			num_of_devices = 1;
			lines_per_device = 1;
			auto_dev_msg = 0;
		}

		if ((dev_access_fct != IFX_NULL) &&
		    (num_of_devices > 0) && (lines_per_device > 0)) {
			DTI_Printf
			    ("Device[%d]: Add GENERIC - devs = %d "
			     "lines per devs = %d\n", dev_if_num,
						      num_of_devices,
						      lines_per_device);

			agent_atartup_settings_xdevs.devIfSettings[dev_if_num].
			    numOfDevices = num_of_devices;
			agent_atartup_settings_xdevs.devIfSettings[dev_if_num].
			    linesPerDevice = lines_per_device;
			agent_atartup_settings_xdevs.devIfSettings[dev_if_num].
			    pDeviceAccessFct = dev_access_fct;
			agent_atartup_settings_xdevs.devIfSettings[dev_if_num].
			    bStartupAutoDevMsgSupport = auto_dev_msg;

			dev_if_num++;
		}
		dev_access_fct = IFX_NULL;
	}
#endif
#endif

	agent_atartup_settings_xdevs.numOfUsedDevIf = dev_if_num;

	if (dti_op_args.dbg_level_device.bSet == IFX_TRUE)
		DTI_DebugLevelSet((IFX_uint32_t) agent_atartup_settings_xdevs.
				  debugLevel);

	if (DTI_AgentStartXDevs(&dti_agent_ctx,
				&agent_atartup_settings_xdevs) == IFX_SUCCESS) {
#if STARTUP_WITH_REGISTERED_DEVICE
#if defined(DEVICE_GPON_ONU)
		if (dti_cli_add(dti_agent_ctx, DRV_IO_GPON_ONU, "ONU",
			        dti_onu_cli_send, 0x10000) == IFX_SUCCESS) {
			DTI_Printf("CLI interface for GPON ONU activated\n");
		}
#endif

#if defined(DEVICE_GPON_OPTIC)
		if (dti_cli_add(dti_agent_ctx, DRV_IO_GPON_OPTIC, "OPTIC",
			        dti_optic_cli_send, 0x10000) == IFX_SUCCESS)
		{
			DTI_Printf("CLI interface for GPON Optic activated\n");
		}
#endif
#endif

		DTI_Printf("Agent started, press any key and Enter to exit\n");

		dti_main_run = 1;
		*dti_agent_ctx_out = dti_agent_ctx;

		return IFX_SUCCESS;
	}

	dti_main_run = 0;

	return IFX_ERROR;
}

/* ==========================================================================
   Global Part
   ========================================================================== */

#if (IFXOS_BYTE_ORDER == IFXOS_LITTLE_ENDIAN)
extern IFX_uint32_t DTI_IoctlDataSwap(IFX_uint32_t cmd, IFX_void_t *pData);

/** Swap packet payload

   \param[in,out] data32 Packet payload
   \param[in]     size   Size of packet payload in 4 byte units (Size / 4)
*/
IFX_void_t dti_packet_payload_swap(IFX_uint32_t *data32, IFX_uint_t size)
{
	IFX_uint_t i;

	for (i = 0; i < size; i++) {
		data32[i] =
		    ((((IFX_uint32_t) (data32[i]) & 0xff000000) >> 24) |
		     (((IFX_uint32_t) (data32[i]) & 0x00ff0000) >> 8) |
		     (((IFX_uint32_t) (data32[i]) & 0x0000ff00) << 8) |
		     (((IFX_uint32_t) (data32[i]) & 0x000000ff) << 24));
	}
}
#endif

/**
   Parse the given argument array into the option control struct.
*/
IFX_void_t dti_agent_opt_arg_parse(IFX_int_t argc, IFX_char_t *argv[])
{
	int parm_no = 0;

	/*
	   For library call
	   If the getopt is used while normal operation, the static and global
	   variables have to be reset before each parse. */
	DTI_MemSet(&dti_op_args, 0x00, sizeof(struct dti_op_arguments));
	DTI_MemSet(dti_str_params, 0x00, sizeof(dti_str_params));
	optind = 0;

	while (1) {
		int option_index = optind ? optind : 1;
		int c;

		/* 1 colon means there is a required parameter */
		/* 2 colons means there is an optional parameter */
		c = getopt_long(argc, argv, DTI_GETOPT_LONG_OPTSTRING,
				dti_long_options, &option_index);

		if (c == -1) {
			if (parm_no == 0) {
				dti_op_args.help.bSet = IFX_TRUE;
				dti_op_args.help.intValue = 1;
			}
			break;
		}

		parm_no++;

		switch (c) {
		case 'h':
			dti_op_args.help.intValue = 1;
			dti_op_args.help.bSet = IFX_TRUE;
			break;
		case 'v':
			GetOptExt_RequiredDigit(optarg,
						&dti_op_args.dbg_level_device,
						"dbg device");
			break;
		case 'w':
			GetOptExt_RequiredDigit(optarg,
						&dti_op_args.num_of_worker,
						"num of worker");
			break;
		case 'f':
			dti_op_args.run_in_forground.intValue = 1;
			dti_op_args.run_in_forground.bSet = IFX_TRUE;
			break;

		case 'l':
			GetOptExt_RequiredDigit(optarg,
						&dti_op_args.lines_per_device,
						"lines");
			break;
		case 'd':
			GetOptExt_RequiredDigit(optarg,
						&dti_op_args.devices,
						"devices");
			break;
		case 'p':
			GetOptExt_RequiredDigit(optarg,
						&dti_op_args.dti_port,
						"port");
			break;

		case 'D':
			GetOptExt_RequiredDigit(optarg,
						&dti_op_args.dev_auto_msg,
						"DevAuto");

			break;
		case 'C':
			GetOptExt_RequiredDigit(optarg,
						&dti_op_args.cli_auto_msg,
						"CliAuto");
			break;

		case 'a':
			GetOptExt_RequiredStr(optarg, &dti_str_params[0],
					      "IP Addr");
			break;
		default:
			break;
		}		/* switch(c) {...} */
	}			/* while(1) {...} */

	if (optind < (argc - 1)) {
		DTI_Printf("Sorry, there are unrecognized options: ");
		while (optind < argc)
			DTI_Printf("%s ", argv[optind++]);

		DTI_Printf("\n");
	}
}

/*
   Check if only the help is requested
*/
IFX_int_t dti_agent_parse_help(void)
{
	if (dti_op_args.help.bSet == 1) {
		dti_agent_do_help();

		return IFX_ERROR;
	}

	return IFX_SUCCESS;
}

#if defined(DTI_LIBRARY)

DTI_AgentCtx_t *global_dti_agent_ctx = IFX_NULL;

IFX_int_t dti_agent_libmain(const IFX_char_t *arg_string)
{
	IFX_int_t ret_val = IFX_SUCCESS, arg_cnt = 0;
	DTI_AgentCtx_t *dti_agent_ctx;

	if (!arg_string) {
		dti_agent_do_help();

		return IFX_SUCCESS;
	}

	DTI_MemSet(dti_control_options, 0x00, sizeof(dti_control_options));
	DTI_MemSet(dti_control_arg_str, 0x00, sizeof(dti_control_arg_str));

	DTI_snprintf(dti_control_arg_str, DTI_OPTIONS_MAX_LEN, "dti %s",
		     arg_string);

	arg_cnt = GetOptExt_ParseArgString(dti_control_arg_str,
					  dti_control_options,
					  DTI_OPTIONS_MAX_NUM);

	if (arg_cnt <= 0) {
		DTI_Printf("Error: DTI Lib - get options <%s>\n", arg_string);

		return IFX_ERROR;
	}

	dti_agent_opt_arg_parse(arg_cnt, dti_control_options);
	if (dti_agent_parse_help() != IFX_SUCCESS)
		return IFX_ERROR;

	if ((ret_val = dti_agent(&dti_agent_ctx)) == IFX_SUCCESS) {
		global_dti_agent_ctx = dti_agent_ctx;

		while (dti_main_run == 1)
			DTI_SecSleep(1);

		DTI_AgentStop(&dti_agent_ctx);
	}

	return ret_val;
}
#endif				/* #if defined(DTI_LIBRARY) */

#if !defined(DTI_LIBRARY)
/* ==========================================================================
   main part
   ========================================================================== */

/* ==========================================================================
   main part - local function declaration
   ========================================================================== */

DTI_STATIC IFX_boolean_t dti_agent_key_get(IFX_char_t *in_key,
					   IFX_char_t *in_key_num);

/* ==========================================================================
   main part - variables
   ========================================================================== */

static IFX_char_t dti_main_in_key = 'c';
static IFX_char_t dti_main_in_key_num = '0';

/* ==========================================================================
   main part - local function definiton
   ========================================================================== */

/**
   Check for for keypressed and terminate
*/
DTI_STATIC IFX_boolean_t dti_agent_key_get(IFX_char_t *in_key,
					   IFX_char_t *in_key_num)
{
	IFX_int_t key = (IFX_int_t) 'c';

	while (1) {
		key = (char)DTI_GetChar();

		if (DTI_IsAlNum(key))
			break;
	}

	if (DTI_IsDigit(key)) {
		if (in_key_num)
			*in_key_num = (IFX_char_t) key;
	}

	if (DTI_IsAlpha(key)) {
		if (in_key)
			*in_key = (IFX_char_t) key;
	}

	if ((IFX_char_t) key == 'q')
		return IFX_TRUE;
	else
		return IFX_FALSE;
}

/* ==========================================================================
   main
   ========================================================================== */

int main(int argc, char *argv[])
{
	IFX_int_t ret_val = IFX_SUCCESS;
	DTI_AgentCtx_t *dti_agent_ctx;

	dti_agent_opt_arg_parse(argc, argv);

	if (dti_agent_parse_help() != IFX_SUCCESS)
		return -1;

#if defined(DEVICE_GPON_ONU) && defined (GPON_ONU_SIMULATION)
	if (ifx_onu_init() != IFX_SUCCESS)
		return -1;
#endif

#if defined(DEVICE_GPON_OCAL) && defined (GPON_OPTIC_SIMULATION)
	if (ifx_optic_init() != IFX_SUCCESS)
		return -1;
#endif

	if (DTI_conInit() != IFX_SUCCESS) {
		DTI_Printf("Error: DTI - Start Agent, Socket init\n");
		return -1;
	}

	if ((ret_val = dti_agent(&dti_agent_ctx)) == IFX_SUCCESS) {
		DTI_Printf("DTI - press <q> for quit" DTI_CRLF);

		while (dti_main_run == 1) {
			if (dti_op_args.run_in_forground.bSet == IFX_FALSE) {
				DTI_SecSleep(1);
			} else {
				(void)dti_agent_key_get(&dti_main_in_key,
						        &dti_main_in_key_num);

				switch (dti_main_in_key) {
				case 'l':
					/* update debug level */
					{
						IFX_uint8_t dbg_level = 0;
						dbg_level =
						    (IFX_uint8_t)
						    (dti_main_in_key_num - '0');
						if ((dbg_level > 0) &&
						    (dbg_level < 5)) {
							DTI_Printf
							    ("DTI - change: "
							     "debug level %d"
								DTI_CRLF,
								dbg_level);
							DTI_DebugLevelSet(
								(IFX_uint32_t)
								    dbg_level);
						}
					}
					break;

				case 'c':
					/* continue */
					break;

#if defined(DEVICE_GPON_OCAL)
				case 'C':
					{
						/* add gpon ocal interface for
						   testing */
						DTI_DeviceIfSettings_t
								     dev_if_ocal;

						dev_if_ocal.numOfDevices = 1;
						dev_if_ocal.linesPerDevice = 1;
						dev_if_ocal.
						    bStartupAutoDevMsgSupport =
						    IFX_TRUE;
						dev_if_ocal.pDeviceAccessFct =
						   &dti_dev_access_fct_gpon_ocal;

						DTI_Printf
						    ("DTI - Add GPON OCAL Device"
						     DTI_CRLF);
						(void)
						    DTI_DeviceConfigAdd
						    (dti_agent_ctx, -1,
						     &dev_if_ocal);
					}
					break;
#endif

#if defined(DEVICE_GPON_ONU)
				case 'O':
					{
						/* add gpon onu interface for
						   testing */
						DTI_DeviceIfSettings_t
								     dev_if_onu;

						dev_if_onu.numOfDevices = 1;
						dev_if_onu.linesPerDevice = 1;
						dev_if_onu.
						    bStartupAutoDevMsgSupport =
						    IFX_TRUE;
						dev_if_onu.pDeviceAccessFct =
						   &dti_dev_access_fct_gpon_onu;

						DTI_Printf
						    ("DTI - Add GPON ONU Device"
						     DTI_CRLF);
						(void)
						    DTI_DeviceConfigAdd
						    (dti_agent_ctx, -1,
						     &dev_if_onu);

						(void)dti_cli_add(dti_agent_ctx,
								 DRV_IO_GPON_ONU,
								 "ONU",
								 dti_onu_cli_send,
								 0x10000);
					}
					break;
#endif

#if defined(DEVICE_GPON_OPTIC)
				case 'o':
					{
						/* add gpon optic interface for
						   testing */
						DTI_DeviceIfSettings_t
								   dev_if_optic;

						dev_if_optic.numOfDevices = 1;
						dev_if_optic.
							   linesPerDevice = 1;
						dev_if_optic.
						    bStartupAutoDevMsgSupport =
						    IFX_TRUE;
						dev_if_optic.pDeviceAccessFct =
						 &dti_dev_access_fct_gpon_optic;

						DTI_Printf
						    ("DTI - Add GPON Optic "
						     "Device" DTI_CRLF);
						(void)
						    DTI_DeviceConfigAdd(
							dti_agent_ctx, -1,
							&dev_if_optic);

						(void)dti_cli_add(dti_agent_ctx,
							DRV_IO_GPON_OPTIC,
							"OPTIC",
							dti_optic_cli_send,
							0x10000);
					}
					break;
#endif

#if defined(DEVICE_GENERIC)
				case 'g':
					{
						/* add generic interface for
						   testing */
						DTI_DeviceIfSettings_t
								dev_if_generic;

						dev_if_generic.
							   numOfDevices = 1;
						dev_if_generic.
							   linesPerDevice = 1;
						dev_if_generic.
						    bStartupAutoDevMsgSupport =
						    IFX_FALSE;
						dev_if_generic.
						   pDeviceAccessFct =
						   &DTI_DeviceAccessFct_GENERIC;

						DTI_Printf
						    ("DTI - Add Generic Device"
						     DTI_CRLF);
						(void)
						    DTI_DeviceConfigAdd(
							      dti_agent_ctx, -1,
							      &dev_if_generic);
					}
					break;
#endif

				case 'b':
					DTI_Printf
					    ("DTI - Switch to Background Mode"
					     DTI_CRLF);
					dti_op_args.run_in_forground.bSet =
					    IFX_TRUE;
					break;

				case 'q':
					DTI_Printf("DTI - Stop Agent" DTI_CRLF);
					dti_main_run = 0;
					break;

				default:
					break;
				}

			}
		}

		(void)DTI_AgentStop(&dti_agent_ctx);
	}
#if defined(DEVICE_GPON_ONU) && defined (GPON_ONU_SIMULATION)
	ifx_onu_exit();
#endif
#if defined(DEVICE_GPON_OCAL) && defined (GPON_OPTIC_SIMULATION)
	ifx_optic_exit();
#endif

	(void)DTI_conCleanup();

	return ret_val;
}

#endif				/* #if !defined(DTI_LIBRARY) */
