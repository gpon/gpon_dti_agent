#ifndef _DTI_AGENT_OCAL_H
#define _DTI_AGENT_OCAL_H
/******************************************************************************

                              Copyright (c) 2011
                            Lantiq Deutschland GmbH
                     Am Campeon 3; 85579 Neubiberg, Germany

  For licensing information, see the file 'LICENSE' in the root folder of
  this software module.

******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif

/** \file
   GPON OCAL device, control struct for GPON Optic Calibration access
   and handling.
*/

/* ==========================================================================
   includes
   ========================================================================== */
#include "ifx_types.h"
#include "ifxos_debug.h"
#include "ifxos_device_access.h"

/* ==========================================================================
   defines
   ========================================================================== */

/** DTI Device Interface Name */
#define DTI_DEV_GPON_OCAL_NAME			"GPON-OCAL"

#define DTI_BOARD_NAME_STR_GPON_OCAL	"GPON Board (OCAL)"

#define DTI_GPON_ONU_START_ID				0
#define DTI_GPON_ONU_MAX_ID				3

/** Max number of register accesses per packet */
#define DTI_DEV_GPON_OCAL_MAX_REGISTER_ACCESS_NUM	0x10
/** Max number of debug accesses per packet */
#define DTI_DEV_GPON_OCAL_MAX_DEBUG_ACCESS_NUM		0x10

/** default GPON OCAL device mailbox size */
#define DTI_DEV_GPON_OCAL_DEFAULT_MBOX_SIZE		0x100

/* ==========================================================================
   macro
   ========================================================================== */

/* ==========================================================================
   types
   ========================================================================== */

/* ==========================================================================
   exports
   ========================================================================== */

#ifdef __cplusplus
}
#endif
#endif				/* _DTI_AGENT_OCAL_H */
